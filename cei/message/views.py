from django.shortcuts import render
from .models.message import *
from django.contrib import messages
import math

def message(request, s_page = '1'):
  if request.method == 'GET' or 'cei_id' not in request.POST:
    messages.info(request, 'ログインしてください。')
    return render(request, 'user_login.html')
  else:
    user_id = request.POST['cei_id']
    user_work = User.objects.filter(id=user_id)
    if user_work.count() == 0:
      messages.info(request, '不正なアクセスです。')
      return render(request, 'user_login.html')
    else:
      if request.POST['post_type'] == 'get_page':
        page = int(s_page)
        message_count = int(math.ceil(Message.objects.filter(user_id=user_id, enabled=True).count() / 10))
        pages = []
        for i in range(message_count):
          pages.append(i + 1)
        message_data = Message.objects.filter(user_id=user_id, enabled=True).values('user__name', 'text', 'send_datetime', 'admin_send_flag').order_by('-send_datetime')[(page - 1) * 10:page * 10]
        message_count = message_data.count()
        message_list = [''] * message_count
        for idx, message in enumerate(message_data):
          message_list[message_count - idx - 1] = message
        Message.objects.filter(user_id=user_id, enabled=True, admin_send_flag=True, read_flag=False).update(read_flag=True)
        return render(request, 'message.html', dict(message_list=message_list, pages=pages))
      else:
        message = Message()
        message.user = User.objects.get(id=user_id)
        message.text = request.POST['text']
        message.admin_send_flag = False
        message.save()
        page = int(s_page)
        message_count = int(math.ceil(Message.objects.filter(user_id=user_id, enabled=True).count() / 10))
        pages = []
        for i in range(message_count):
          pages.append(i + 1)
        message_data = Message.objects.filter(user_id=user_id, enabled=True).values('user__name', 'text', 'send_datetime', 'admin_send_flag').order_by('-send_datetime')[(page - 1) * 10:page * 10]
        message_count = message_data.count()
        message_list = [''] * message_count
        for idx, message in enumerate(message_data):
          message_list[message_count - idx - 1] = message
        return render(request, 'message.html', dict(message_list=message_list, pages=pages))
