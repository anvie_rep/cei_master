from __future__ import unicode_literals
from django.contrib import admin
from message.models.message import *

class MessageAdmin(admin.ModelAdmin):
  list_display = (
     'id', 'user', 'admin_send_flag', 'enabled'
  )
  ordering = ('id',)

admin.site.register(Message, MessageAdmin)