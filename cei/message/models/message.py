from django.db import models
from userManagement.models.user import *

class Message(models.Model):
  user = models.ForeignKey(User)
  text = models.CharField(max_length=8192, null=False, blank=False)
  send_datetime = models.DateTimeField(auto_now=False, auto_now_add=True)
  admin_send_flag = models.BooleanField()
  read_flag = models.BooleanField(default=False)
  enabled = models.BooleanField(default=True)

  def __str__(self):
    return "{} {}".format(self.user, self.admin_send_flag, self.text[:20])
