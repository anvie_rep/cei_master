$(function(){
  var document_title = document.title;
  if(document_title == "新規カテゴリー") {
    $('#id_category_color').addClass('color {hash:true}');
  }
  if(document_title == "管理者ログイン"){
    if(localStorage.getItem("admin_id") && localStorage.getItem("admin_pass")){
      postForm();
    }
  }
  if(document_title == "ユーザーログイン"){
    if(localStorage.getItem("cei_id") && localStorage.getItem("cei_pass")){
      postForm();
    }
  }
  if(document_title == "メッセージ"){
    insertUserInfo();
  }
  if(document_title == "ニュースリスト"){
    insertUserInfo();
  }
  if(document_title == "ニュース詳細"){
    insertUserInfo();
  }
  if(document_title == "ユーザー情報編集"){
    insertUserInfo();
  }
  if(document_title == "ユーザー作成"){
    insertUserInfo();
  }
});

function insertUserInfo() {
  var cei_id = localStorage.getItem("cei_id");
  var cei_name = localStorage.getItem("cei_name");
  var cei_rank_name = localStorage.getItem("cei_rank_name");
  var cei_rank_color = localStorage.getItem("cei_rank_color");
  if(cei_id && cei_name && cei_rank_name && cei_rank_color) {
    $.each(document.getElementsByClassName('input-cei-id'), function(){
      $(this).val(cei_id);
    });
    $.each(document.getElementsByClassName('insert-user-name'), function(){
      $(this).text(cei_name);
    });
    $.each(document.getElementsByClassName('insert-user-rank-name'), function(){
      $(this).text(cei_rank_name);
    });
    $.each(document.getElementsByClassName('insert-user-rank-color'), function(){
      $(this).css({
        'color':cei_rank_color,
        'border-color':cei_rank_color
      });
    });
  }
}