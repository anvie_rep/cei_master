from __future__ import unicode_literals
from django.contrib import admin
from news.models.news import *
from news.models.category import *

class NewsAdmin(admin.ModelAdmin):
  list_display = (
     'id', 'title', 'enabled',
  )
  list_display_links = ('title', )

admin.site.register(News, NewsAdmin)

class CategoryAdmin(admin.ModelAdmin):
  list_display = (
     'id', 'category_name', 'category_color',
  )

admin.site.register(Category, CategoryAdmin)
