from django.shortcuts import render
import math
from .forms import *
from userManagement.models.user import *
from django.contrib import messages

def newsList(request, s_page = '1'):
  if request.method == 'GET' or 'cei_id' not in request.POST:
    messages.info(request, 'ログインしてください。')
    return render(request, 'user_login.html')
  else:
    user_id = request.POST['cei_id']
    user_work = User.objects.filter(id=user_id)
    if user_work.count() == 0:
      messages.info(request, '不正なアクセスです。')
      return render(request, 'user_login.html')
    else:
      page = int('0' + s_page)
      news_data = News.objects.filter(watch_flg=True, enabled = True).order_by('-post_datetime')
      last_page = int(math.ceil(news_data.count() / 6))
      pages = []
      for i in range(last_page):
        pages.append(i + 1)
      news_list = news_data[(page - 1) * 6:page * 6].values('post_datetime', 'news_category__category_name', 'title', 'news_category__category_color', 'id')
      return render(request, 'news_list.html', dict(news_list=news_list, pages=pages))


def newsSingle(request, id = None):
  if request.method == 'GET' or 'cei_id' not in request.POST:
    messages.info(request, 'ログインしてください。')
    return render(request, 'user_login.html')
  else:
    user_id = request.POST['cei_id']
    user_work = User.objects.filter(id=user_id)
    if user_work.count() == 0:
      messages.info(request, '不正なアクセスです。')
      return render(request, 'user_login.html')
    else:
      if id:
        news = News.objects.filter(id = id, watch_flg=True, enabled = True).values('post_datetime', 'news_category__category_name', 'caption', 'title', 'news_category__category_color')[0]
        if news:
          return render(request, 'news_single.html', dict(news=news))
        else:
          messages.info(request, 'ニュースが存在しません。')
          return render(request, 'news_list.html')
      else:
          messages.info(request, '不正なアクセスです。')
          return render(request, 'news_list.html')

