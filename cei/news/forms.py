from django.forms import ModelForm, RadioSelect, Textarea, ValidationError, HiddenInput, BooleanField
from .models.news import *
from django.forms.widgets import TextInput

class newsForm(ModelForm):
  class Meta:
    model = News
    fields = (
      'news_category', 'title', 'caption', 'watch_flg',
    )
    widgets = {
      'caption': Textarea,
      'watch_flg' : RadioSelect(choices=((True, 'ON',), (False, 'OFF',)))
    }

  def __init__(self, *args, **kwargs):
    kwargs.setdefault('label_suffix', '')
    super(newsForm, self).__init__(*args, **kwargs)


class categoryForm(ModelForm):
  class Meta:
    model = Category
    fields = (
      'category_name', 'category_color',
    )

  def __init__(self, *args, **kwargs):
    kwargs.setdefault('label_suffix', '')
    super(categoryForm, self).__init__(*args, **kwargs)
    # self.fields['category_name'].widget.attrs.update({'class': 'color'})