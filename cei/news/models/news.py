from django.db import models
from news.models.category import *

class News(models.Model):
  # news_image = models.ImageField()
  news_category = models.ForeignKey(Category, null=False, blank=False, default=0)
  title = models.CharField(max_length=256, null=False, blank=False)
  caption = models.CharField(max_length=8192, null=False, blank=False)
  views = models.CharField(max_length=16)
  post_datetime = models.DateTimeField(auto_now=False, auto_now_add=True)
  watch_flg = models.BooleanField(default=True)
  enabled = models.BooleanField(default=True)

  def __str__(self):
    return "{} {}".format(self.title, self.caption[:20])