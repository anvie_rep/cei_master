from django.db import models
from colorfield.fields import ColorField

class Category(models.Model):
  category_name = models.CharField(max_length=16, null=False, blank=False)
  category_color = ColorField(max_length=8, null=False, blank=False)

  def __str__(self):
    return self.category_name