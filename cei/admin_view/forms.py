from django.forms import ModelForm, RadioSelect, Textarea, ValidationError, HiddenInput, BooleanField
from .models.admin import *
from django.forms.widgets import TextInput

class adminForm(ModelForm):
  class Meta:
    model = Admin
    fields = (
      'admin_id', 'admin_pass'
    )
    widgets = {
    }

  def __init__(self, *args, **kwargs):
    kwargs.setdefault('label_suffix', '')
    super(adminForm, self).__init__(*args, **kwargs)