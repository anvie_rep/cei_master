from django.shortcuts import render, redirect, get_object_or_404
import math
from .forms import *
from userManagement.forms import *
from news.forms import *
from message.models.message import *
from django.contrib import messages

# admin_login

def adminTop(request, s_page = '1'):
  if request.method == 'GET':
    messages.info(request, 'ログインしてください。')
    return redirect('admin_view:adminLogin')
  else:
    admin_id = request.POST['admin_id']
    admin_pass = request.POST['admin_pass']
    admin = Admin.objects.filter(admin_id=admin_id, admin_pass=admin_pass).values('admin_id', 'admin_pass')
    if admin:
      page = int('0' + s_page)
      message_data = Message.objects.filter(enabled=True, read_flag=False, admin_send_flag=False).order_by(
        '-send_datetime')
      last_page = math.floor(message_data.count() / 6) + 1
      pages = []
      for i in range(last_page):
        pages.append(i + 1)
      message_list = message_data[(page - 1) * 6:page * 6].values('user__id', 'user__name', 'text', 'send_datetime',
                                                                  'admin_send_flag', 'read_flag')
      return render(request, 'admin_top.html', dict(message_list=message_list, pages=pages))
    else:
      messages.info(request, 'ユーザーIDまたはパスワードが間違っています。')
      return render(request, 'admin_login.html')


def adminLogin(request):
  if request.method == 'GET':
    return render(request, 'admin_login.html')
  else:
    admin_id = request.POST['admin_id']
    admin_pass = request.POST['admin_pass']
    admin = Admin.objects.filter(admin_id=admin_id, admin_pass=admin_pass).values('admin_id', 'admin_pass')
    if admin:
      return render(request, 'admin_dummy_top.html', dict(lsStore = admin.values('admin_id', 'admin_pass')[0]))
    else:
      messages.info(request, 'ユーザーIDまたはパスワードが間違っています。', dict(lsStore ={'admin_id':'', 'admin_pass':''}))
      return render(request, 'admin_login.html')


# admin_news

def newsListAdmin(request, s_page = '1'):
  if request.method == 'GET':
    messages.info(request, 'ログインしてください。')
    return redirect('admin_view:adminLogin')
  else:
    page = int('0' + s_page)
    news_data = News.objects.filter(enabled = True).order_by('-post_datetime')
    last_page = math.floor(news_data.count() / 6) + 1
    pages = []
    for i in range(last_page):
      pages.append(i + 1)
    news_list = news_data[(page - 1) * 6:page * 6].values('post_datetime', 'news_category__category_name', 'title', 'news_category__category_color', 'id')
    return render(request, 'admin_news_list.html', dict(news_list=news_list, pages=pages))


def categoryList(request):
  if request.method == 'GET':
    messages.info(request, 'ログインしてください。')
    return redirect('admin_view:adminLogin')
  else:
    # page = int('0' + s_page)
    category_list = Category.objects.all().order_by('id')
    # last_cate_page = math.floor(category_data.count() / 6) + 1
    pages_cat = []
    # for i in range(last_cate_page):
    #   pages_cat.append(i + 1)
    # category_list = category_data[(page - 1) * 6:page * 6].values('category_name', 'category_color', 'id')
    return render(request, 'admin_category_list.html', dict(category_list=category_list))


def newsSingleAdmin(request, id = None):
  if request.method == 'GET':
    messages.info(request, 'ログインしてください。')
    return render(request, 'admin_login.html')
  else:
    if id:
      news = News.objects.filter(id = id, enabled = True).values('id', 'post_datetime', 'news_category__category_name', 'caption', 'title', 'news_category__category_color')[0]
      if news:
        return render(request, 'admin_news_single.html', dict(news=news))
      else:
        messages.info(request, 'ニュースが存在しません。')
        return render(request, 'admin_news_list.html')
    else:
        messages.info(request, '不正なアクセスです。')
        return render(request, 'admin_news_list.html')

def categoryEdit(request, id = None):
  if request.method == "GET":
    messages.info(request, 'ログインしてください。')
    return render(request, 'admin_login.html')
  else:
    if request.POST['post_type'] == 'get_page':
      if id:
        category = get_object_or_404(Category, pk=id)
      else:
        category = Category()
      form = categoryForm(instance=category)
      return render(request, 'admin_category_create.html', dict(form=form, id=id))
    else:
      if id:
        category = get_object_or_404(Category, pk=id)
      else:
        category = Category()
      form = categoryForm(request.POST, instance=category)
      print('1')
      print(form)
      if form.is_valid():
        category = form.save(commit=False)
        category.save()
        return render(request, 'admin_dummy_category.html')
      else:
        messages.info(request, '入力に誤りがあります。')
        return render(request, 'admin_category_create.html', dict(form=form, id=id))


def newsEdit(request, id=None):
  if request.method == "GET":
    messages.info(request, 'ログインしてください。')
    return render(request, 'admin_login.html')
  else:
    if request.POST['post_type'] == 'get_page':
      if id:
        news = get_object_or_404(News, pk=id)
      else:
        news = News()
      form = newsForm(instance=news)
      return render(request, 'admin_news_create.html', dict(form=form, id=id))
    else:
      if id:
        news = get_object_or_404(News, pk=id)
      else:
        news = News()
      form = newsForm(request.POST, instance=news)
      if form.is_valid():
        news = form.save(commit=False)
        news.enabled = True
        news.save()
        return render(request, 'admin_dummy_news.html')
      else:
        messages.info(request, '入力に誤りがあります。')
        return render(request, 'admin_news_create.html', dict(form=form, id=id))


def newsDelete(request, id=None):
  if request.method == "GET":
    messages.info(request, 'ログインしてください。')
    return render(request, 'admin_login.html')
  else:
    if id:
      try:
        news = News.objects.get(id=id, enabled=True)
      except News.DoesNotExist:
        messages.info(request, '選択されたニュースは存在しません。')
        return redirect('admin_view:adminLogin')
      else:
        news.enabled = False
        news.save()
        messages.info(request, '選択されたニュースは削除されました。')
        return render(request, 'admin_dummy_news.html')
    else:
      messages.info(request, '不正なアクセスです。')
      return redirect('admin_view:adminLogin')


def adminUserEdit(request, user_id=None):
  if request.method == 'GET':
    messages.info(request, 'ログインしてください。')
    return render(request, 'admin_login.html')
  else:
    if request.POST['post_type'] == 'get_page':
      if user_id:
        user = get_object_or_404(User, id=user_id)
        form = userEditForm(instance=user)
      else:
        user = User()
        form = userCreateForm(instance=user)
      return render(request, 'admin_user_edit.html', dict(form=form, user_id=user_id))
    else:
      if user_id:
        user = get_object_or_404(User, id=user_id)
        form = userEditForm(request.POST, instance=user)
      else:
        user = User()
        form = userCreateForm(request.POST, instance=user)
      if form.is_valid():
        user = form.save(commit=False)
        user.save()
        return render(request, 'admin_dummy_user_single.html', dict(user_id=user.id))
      else:
        return render(request, 'admin_user_edit.html', dict(form=form))



def adminUserSingle(request, user_id=None):
  if user_id:
    if request.method == 'GET':
      messages.info(request, 'ログインしてください。')
      return render(request, 'admin_login.html')
    else:
      if request.POST['post_type'] == 'get_page':
        user = User.objects.filter(id=user_id).values('id', 'name', 'phone', 'mail', 'rank__name', 'company', 'password', 'register_datetime', 'last_login_datetime', 'enabled')[0]
        return render(request, 'admin_user_single.html', dict(user=user))
      else:
        messages.info(request, '不正なアクセスです。')
        return render(request, 'admin_login.html')
  else:
    messages.info(request, '不正なアクセスです。')
    return render(request, 'admin_login.html')


def adminUserList(request):
  if request.method == "GET":
    messages.info(request, 'ログインしてください。')
    return render(request, 'admin_login.html')
  else:
    user_list = User.objects.filter(enabled=True).values('id', 'name', 'company', 'last_login_datetime', 'rank__name', 'rank__color').order_by('id')
    return render(request, 'admin_user_list.html', dict(user_list=user_list))


def adminUserDelete(request, user_id=None):
  if request.method == 'GET':
    messages.info(request, 'ログインしてください。')
    return render(request, 'admin_login.html')
  else:
    if user_id:
      try:
        user = User.objects.get(id=user_id)
      except User.DoesNotExist:
        messages.info(request, '選択されたユーザーは存在しません。')
        return render(request, 'admin_dummy_user_list.html')
      else:
        user.enabled = False
        user.save()
        messages.info(request, '選択されたユーザーを削除しました。')
        return render(request, 'admin_dummy_user_list.html')
    else:
      messages.info(request, '不正なアクセスです。')
      return render(request, 'admin_dummy_top.html')


def adminUserActivate(request, user_id=None):
  if request.method == 'GET':
    messages.info(request, 'ログインしてください。')
    return render(request, 'admin_login.html')
  else:
    if user_id:
      try:
        user = User.objects.get(id=user_id)
      except User.DoesNotExist:
        messages.info(request, '選択されたユーザーは存在しません。')
        return render(request, 'admin_dummy_user_list_deleted.html')
      else:
        user.enabled = True
        user.save()
        messages.info(request, '選択されたユーザーを復活しました。')
        return render(request, 'admin_dummy_user_list_deleted.html')
    else:
      messages.info(request, '不正なアクセスです。')
      return render(request, 'admin_dummy_top.html')


def adminUserListDeleted(request):
  if request.method == 'GET':
    messages.info(request, 'ログインしてください。')
    return render(request, 'admin_login.html')
  else:
    user_list = User.objects.filter(enabled=False).values('id', 'name', 'company', 'last_login_datetime', 'rank__name', 'rank__color').order_by('id')
    return render(request, 'admin_user_list_deleted.html', dict(user_list=user_list))

def adminUserSearch(request):
  if request.method == 'GET':
    messages.info(request, 'ログインしてください。')
    return render(request, 'admin_login.html')
  else:
    enabled = bool(request.POST['enabled'])
    rank = request.POST['rank']
    company = request.POST['company']
    name = request.POST['name']
    user_list = User.objects.filter(enabled=enabled, rank__name__icontains=rank, company__icontains=company, name__icontains=name).values('id', 'name', 'company', 'last_login_datetime', 'rank__name', 'rank__color').order_by('id')
    if enabled:
      return render(request, 'admin_user_list.html', dict(user_list=user_list))
    else:
      return render(request, 'admin_user_list_deleted.html', dict(user_list=user_list))


def adminMessage(request, user_id = None, s_page = '1'):
  if user_id:
    if request.method == "GET":
      messages.info(request, 'ログインしてください。')
      return render(request, 'admin_login.html')
    else:
      if request.POST['post_type'] == 'get_page':
        page = int(s_page)
        message_count = int(math.ceil(Message.objects.filter(user_id=user_id, enabled=True).count() / 10))
        pages = []
        for i in range(message_count):
          pages.append(i + 1)
        message_data = Message.objects.filter(user_id=user_id, enabled=True).values('user__name', 'text', 'send_datetime', 'admin_send_flag', 'read_flag').order_by('-send_datetime')[(page - 1) * 10:page * 10]
        message_count = message_data.count()
        message_list = [''] * message_count
        for idx, message in enumerate(message_data):
          message_list[message_count - idx - 1] = message
        Message.objects.filter(user_id=user_id, enabled=True, admin_send_flag=False, read_flag=False).update(read_flag=True)
        return render(request, 'admin_message.html', dict(message_list=message_list, user_id=user_id, pages=pages))
      else:
        message = Message()
        message.user = User.objects.get(id=user_id)
        message.text = request.POST['text']
        message.admin_send_flag = True
        message.save()
        page = int(s_page)
        message_count = int(math.ceil(Message.objects.filter(user_id=user_id, enabled=True).count() / 10))
        pages = []
        for i in range(message_count):
          pages.append(i + 1)
        message_data = Message.objects.filter(user_id=user_id, enabled=True).values('user__name', 'text', 'send_datetime', 'admin_send_flag', 'read_flag').order_by('-send_datetime')[(page - 1) * 10:page * 10]
        message_count = message_data.count()
        message_list = [''] * message_count
        for idx, message in enumerate(message_data):
          message_list[message_count - idx - 1] = message
        return render(request, 'admin_message.html', dict(message_list=message_list, user_id=user_id, pages=pages))
  else:
    messages.info(request, '不正なアクセスです。')
    return render(request, 'admin_login.html')


def adminMessageList(request, s_page = '1'):
  if request.method == "GET":
    messages.info(request, 'ログインしてください。')
    return render(request, 'admin_login.html')
  else:
    page = int('0' + s_page)
    message_data = Message.objects.filter(enabled=True, read_flag=False, admin_send_flag=False).order_by('-send_datetime')
    last_page = math.floor(message_data.count() / 6) + 1
    pages = []
    for i in range(last_page):
      pages.append(i + 1)
    message_list = message_data[(page - 1) * 6:page * 6].values('user__id', 'user__name', 'text', 'send_datetime', 'admin_send_flag', 'read_flag')
    return render(request, 'admin_message_list.html', dict(message_list=message_list, pages=pages))