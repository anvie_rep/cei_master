from __future__ import unicode_literals
from django.contrib import admin
from admin_view.models.admin import *


class adminAdmin(admin.ModelAdmin):
  list_display = (
    'admin_id', 'admin_pass',
  )

admin.site.register(Admin, adminAdmin)
# Register your models here.
