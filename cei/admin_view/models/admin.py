from django.db import models

class Admin(models.Model):
  admin_id = models.CharField(primary_key=True, max_length=16, null=False, blank=False)
  admin_pass = models.CharField(max_length=16, null=False, blank=False)
  enabled = models.BooleanField(default=True)