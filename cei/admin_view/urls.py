from django.conf.urls import url
from admin_view import views, templates


urlpatterns = [
  url(r'^$', views.adminTop, name='adminTop'),
  url(r'^admin_login/$', views.adminLogin, name='adminLogin'),
  url(r'^user_list/$', views.adminUserList, name='adminUserList'),
  url(r'^user_create/$', views.adminUserEdit, name='adminUserCreate'),
  url(r'^user_single/(?P<user_id>.{1,16})/$', views.adminUserSingle, name='adminUserSingle'),
  url(r'^user_edit/(?P<user_id>.{1,16})/$', views.adminUserEdit, name='adminUserEdit'),
  url(r'^user_delete/(?P<user_id>.{1,16})/$', views.adminUserDelete, name='adminUserDelete'),
  url(r'^user_activate/(?P<user_id>.{1,16})/$', views.adminUserActivate, name='adminUserActivate'),
  url(r'^user_list_deleted/$', views.adminUserListDeleted, name='adminUserListDeleted'),
  url(r'^user_list_search/$', views.adminUserSearch, name='adminUserSearch'),
  url(r'^news_list_admin/$', views.newsListAdmin, name='newsListAdmin'),
  url(r'^news_list_admin/(?P<s_page>\d+)/$', views.newsListAdmin, name='newsListWithPageAdmin'),
  url(r'^news_single_admin/(?P<id>.+)/$', views.newsSingleAdmin, name='newsSingleAdmin'),
  url(r'^news_create/$', views.newsEdit, name='newsCreate'),
  url(r'^news_edit/(?P<id>.+)/$', views.newsEdit, name='newsEdit'),
  url(r'^news_delete/(?P<id>.+)/$', views.newsDelete, name='newsDelete'),
  url(r'^category_list/$', views.categoryList, name='categoryList'),
  url(r'^category_create/$', views.categoryEdit, name='categoryCreate'),
  url(r'^category_edit/(?P<id>.+)/$', views.categoryEdit, name='categoryEdit'),
  url(r'^message/(?P<user_id>\w+)/$', views.adminMessage, name="adminMessage"),
  url(r'^message/(?P<user_id>\w+)/(?P<s_page>\d+)/$', views.adminMessage, name="adminMessageWithPage"),
  url(r'^message_list/$', views.adminMessageList, name="adminMessageList"),
  url(r'^message_list/(?P<s_page>\d+)/$', views.adminMessageList, name='adminMessageListWithPage'),
]