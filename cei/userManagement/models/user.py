from django.db import models
from userManagement.models.rank import *


class User(models.Model):
  id = models.CharField("ユーザーID", max_length=32, primary_key=True)
  name = models.CharField("ユーザーネーム", max_length=16, blank=False, null=False)
  password = models.CharField("パスワード", max_length=16, blank=False, null=False)
  phone = models.CharField("電話番号", max_length=13, unique=True, blank=True, null=True)
  mail = models.EmailField("メール", max_length=128, unique=True, blank=True, null=True)
  rank = models.ForeignKey(Rank, verbose_name="ランク", blank=False, null=False)
  company = models.CharField("会社名", max_length=128, blank=True, null=True)
  register_datetime = models.DateTimeField(auto_now=False, auto_now_add=True, blank=True, null=True)
  unregister_datetime = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
  last_login_datetime = models.DateTimeField(auto_now=False, auto_now_add=False, blank=True, null=True)
  enabled = models.BooleanField(default=True)

  def __str__(self):
    return "{} {}".format(self.id, self.name)
