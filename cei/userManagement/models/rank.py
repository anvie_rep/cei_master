from django.db import models
from colorfield.fields import ColorField

class Rank(models.Model):
  name = models.CharField("ランク名", max_length=1)
  color = ColorField(max_length=8, null=False, blank=False)

  def __str__(self):
    return self.name
