from django.forms import ModelForm, ValidationError, HiddenInput
from .models.user import *
from django.core.validators import validate_email
import re

class userCreateForm(ModelForm):
  class Meta:
    model = User
    fields = (
      'id', 'name', 'password', 'phone', 'mail', 'rank', 'company', 'enabled',
    )
    widgets = {
      'enabled' : HiddenInput
    }


  def clean_id(self):
    id = self.cleaned_data["id"]
    alnum_Reg = re.compile(r'^[a-zA-Z0-9_]+$')
    if alnum_Reg.match(id) == None:
      raise ValidationError("英数のみでIDを入力してください")
    try:
      User.objects.get(id=id)
    except User.DoesNotExist:
      return id
    else:
      raise ValidationError("このIDは既に使用されています。別のIDを指定してください")

  def clean_phone(self):
    phone = self.cleaned_data["phone"]

    if phone == "" or phone == None:
      return phone

    try:
      User.objects.get(phone=phone)
    except User.DoesNotExist:
      return phone
    else:
      raise ValidationError("この電話番号は既に使用されています。別の電話番号を指定してください")

  def clean_mail(self):
    mail = self.cleaned_data["mail"]

    if mail == "" or mail == None:
      return mail

    try:
      validate_email(mail)
    except ValidationError:
      raise ValidationError("正しいメールアドレスを指定してください。")

    if User.objects.filter(mail=mail).count() == 0:
      return mail
    else:
      raise ValidationError("このメールアドレスは既に使用されています。別のメールアドレスを指定してください")

  def __init__(self, *args, **kwargs):
    kwargs.setdefault('label_suffix', '')
    super(userCreateForm, self).__init__(*args, **kwargs)

class userEditForm(ModelForm):
  class Meta:
    model = User
    fields = (
      'id', 'name', 'password', 'phone', 'mail', 'rank', 'company', 'enabled',
    )
    widgets = {
      'id' : HiddenInput,
      'enabled' : HiddenInput
    }

  def clean_phone(self):
    id = self['id'].value()
    phone = self.cleaned_data["phone"]

    if phone == "" or phone == None:
      return phone

    if User.objects.filter(phone=phone).exclude(id=id).count() == 0:
      return phone
    else:
      raise ValidationError("この電話番号は既に使用されています。別の電話番号を指定してください")

  def clean_mail(self):
    id = self['id'].value()
    mail = self.cleaned_data["mail"]

    if mail == "" or mail == None:
      return mail

    try:
      validate_email(mail)
    except ValidationError:
      raise ValidationError("正しいメールアドレスを指定してください。")

    if User.objects.filter(mail=mail).exclude(id=id).count() == 0:
      return mail
    else:
      raise ValidationError("このメールアドレスは既に使用されています。別のメールアドレスを指定してください")

  def __init__(self, *args, **kwargs):
    kwargs.setdefault('label_suffix', '')
    super(userEditForm, self).__init__(*args, **kwargs)
