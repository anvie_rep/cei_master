# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models.user import *


class UserAdmin(admin.ModelAdmin):
  list_display = (
    'id', 'name', 'password', 'phone', 'mail', 'rank', 'company', 'register_datetime', 'unregister_datetime',
    'last_login_datetime', 'enabled'
  )
  ordering = ('id',)

admin.site.register(User, UserAdmin)


class RankAdmin(admin.ModelAdmin):
  list_display = (
    'id', 'name'
  )
  ordering = ('id',)

admin.site.register(Rank, RankAdmin)