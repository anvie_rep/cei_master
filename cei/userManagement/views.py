from django.shortcuts import render, redirect, get_object_or_404
from .forms import *
from django.contrib import messages
from datetime import datetime


def userLogin(request):
  if request.method == 'GET':
    return render(request, 'user_login.html')
  else:
    user_id = request.POST['cei_id']
    user_pass = request.POST['cei_pass']
    user_work = User.objects.filter(id=user_id, password=user_pass).values('id', 'password', 'name', 'rank__name', 'rank__color')
    if user_work:
      user = user_work.filter(enabled=True)
      if user:
        lsStore = {
          'cei_id': user[0]['id'],
          'cei_pass': user[0]['password'],
          'cei_name': user[0]['name'],
          'cei_rank_name': user[0]['rank__name'],
          'cei_rank_color': user[0]['rank__color'],
        }
        User.objects.filter(id=user_id).update(last_login_datetime = datetime.now())
        return render(request, 'user_dummy_login.html', dict(lsStore=lsStore))
      else:
        messages.info(request, '会員権の有効期限が切れています。')
        return render(request, 'user_login.html')
    else:
      messages.info(request, 'ユーザーIDまたはパスワードが間違っています。')
      return render(request, 'user_login.html')


def userEdit(request):
  if request.method == 'GET' or 'cei_id' not in request.POST:
    messages.info(request, 'ログインしてください。')
    return render(request, 'user_login.html')
  else:
    user_id = request.POST['cei_id']
    user_work = User.objects.filter(id=user_id)
    if user_work.count() == 0:
      messages.info(request, '不正なアクセスです。')
      return render(request, 'user_login.html')
    else:
      user = user_work[0]
      if request.POST['post_type'] == 'get_page':
        form = userEditForm(instance=user)
        return render(request, 'user_edit.html', dict(form=form, user_id=user.id))
      else:
        form = userEditForm(request.POST, instance=user)
        if form.is_valid():
          user = form.save(commit=False)
          user.save()
          return render(request, 'dummy_user_single.html', dict(user_id=user.id))
        else:
          return render(request, 'user_edit.html', dict(form=form, user_id=user.id))


def userSingle(request):
  if request.method == 'GET' or 'cei_id' not in request.POST:
    messages.info(request, 'ログインしてください。')
    return render(request, 'user_login.html')
  else:
    user_id = request.POST['cei_id']
    user_work = User.objects.filter(id=user_id)
    if user_work.count() == 0:
      messages.info(request, '不正なアクセスです。')
      return render(request, 'user_login.html')
    else:
      if request.POST['post_type'] == 'get_page':
        user = user_work.values('id', 'name', 'phone', 'mail', 'rank__name', 'company', 'password', 'register_datetime')[0]
        return render(request, 'user_single.html', dict(user=user))
      else:
        messages.info(request, '不正なアクセスです。')
        return render(request, 'user_login.html')

